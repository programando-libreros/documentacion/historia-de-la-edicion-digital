El presente artículo solo es para fines de divulgación.

Última revisión: 28 de septiembre de 2016.

# Historia de la edición digital

Ramiro Santa Ana Anguiano

# Irrupción digital

A mediados de los años ochenta, el *software* para procesar textos pasó
de ser una ficción a una realidad cada vez más imprescindible en el
quehacer administrativo. La variedad de programas parecía no mostrar un
competidor sobresaliente. Nada más distante a nuestros días. Quién diría
que en cuestión de años este mercado sería eclipsado por dos programas,
WordPerfect y Word, para, más adelante, sentar el monopolio del último,
ayudando a la consolidación de Microsoft como el principal proveedor de
*software*.

El ambiente se veía prometedor, aunque el estado de la tecnología
todavía no podía competir con la calidad de edición y de impresión que
se obtenían a través de los métodos tradicionales que vinieron
mejorándose desde Gutenberg.

Los más futuristas imaginaron cómo las computadoras sustituirían a
quienes cuidaban de los textos, incluso a quienes escribían. Los más
conservadores temían que este advenimiento tecnológico destruyese u
olvidase una tradición de quinientos años. Los más entusiasmados con
vender esta clase de *software* incluso hablaban de una «liberación
femenina», ya que las secretarias podrían concentrarse a otras tareas
más relevantes y menos monótonas.

\[Imagen processedworld01.jpg con la leyenda: Portada del primer número
de *Processed World*, 1981. Fuente: [Internet
Archive](http://archive.org/search.php?query=creator%3A%22Processed%20World%20Collective%22%20AND%20%28Processed%20World%29).\]

Quizá no era una utopía, aunque aún ahora no es patente. Tal vez solo
era una quimera que bebía de una errónea concepción de lo que significa
«cuidar» un texto. Probablemente, era un cambio de paradigma que
dictaría otra dirección para los procesos de edición. Posiblemente solo
se trataba de un eslogan publicitario *ad hoc* a las corrientes
progresistas de la época. ¿Una revolución? Por supuesto. ¿Un nuevo
comienzo? Tal vez. Mejor regresemos unas décadas para analizar la
edición digital en perspectiva.

# Gestación

La posibilidad de tener un registro editable y reproducible de la manera
más simple y centralizada posible es un anhelo tan antiguo como la
escritura. Pero no será hasta la invención de la imprenta y de los tipos
que este sueño empieza a tener un camino más claro. Bajo estos mismos
ideales, a partir de los cincuenta, es como comienza a cuajar la
tecnología necesaria para la creación de formatos más accesibles para
guardar estos registros. Del metal se empezó la transición hacia las
tarjetas perforadas y las cintas magnéticas.

Durante esa época, el modelo a seguir para la reproducción de textos no
vino de la tradición editorial, sino del mecanismo de las pianolas. La
idea básica consistía en tener un rollo con la información «grabada» y
fácil de reproducir en cualquier otra máquina. Si en la pianola estos
rollos sirven para tocar música, las máquinas de escribir magnéticas
ofrecieron la posibilidad de «guardar» el texto en una cinta que después
podía utilizarse para su edición o impresión.

\[Imagen IBMpublicidad.jpg con la leyenda: Publicidad de IBM, *c*. 1964.
Fuente: [IBM Archives](http://www-03.ibm.com/ibm/history/index.html).\]

En realidad, uno de los elementos más novedosos fue el surgimiento del
término *procesador de texto*. Este concepto lo forjó un empleado de IBM
durante los sesenta, con el cual se hacía referencia a una tarea muy
específica dentro del «procesamiento de datos» y el sueño de la
«automatización de oficinas» (también conocida como *ofimática*): el
dictado y el mecanografiado de la información. Lo que se pretendía era
la automatización de una tarea administrativa, un quehacer muy distinto
al cuidado editorial y la producción de libros.

# Nacimiento

Sin embargo, será recién en los setentas cuando este concepto se
populariza, marcando la tendencia entre las empresas dedicadas a la
creación de equipo para las oficinas. Sin dudas, IBM fue la principal
compañía que se dedicó a desarrollar esta metodología de trabajo. El
procesador de texto no hacía referencia a un *software*, sino a un
*hardware*: una máquina que se dedicaba a realizar exclusivamente esta
tarea.

\[Imagen IBMmtst.jpg con la leyenda: Publicidad de la máquina IBM MT/ST,
*c*. 1964. Fuente: [IBM
Archives](http://www-03.ibm.com/ibm/history/index.html).\]

Si bien las ventas de los procesadores de texto continuaron incentivando
la creación de nuevos modelos, sus altos precios, la poca versatilidad y
la necesidad de personal altamente capacitado sentaron las bases para su
perdición. Gracias a los avances en el desarrollo de *software*, a la
caída de los precios del *hardware*, al mejoramiento de las impresoras
«personales» y a la estandarización de las unidades de almacenaje (los
disquetes), las microcomputadoras se hicieron accesibles como máquinas
de oficina o equipos personales durante los años ochenta.

La capacidad de desarrollo de *software* aumentó exponencialmente cuando
desde las computadoras se pudo programar con lenguajes de programación.
Parece inverosímil, pero durante algún tiempo la programación de
computadoras se realizaba directamente con la redacción de cadenas de
ceros y unos, también conocido como *código máquina*. Con el fin de
acelerar y simplificar esta tarea surgieron los lenguajes de
programación. Evidentemente, esto también provocó la creación de un
*software* diseñado para la escritura. Antes de los editores de código,
los cuales asisten al programador en su labor, los primeros editores
fueron de texto, algo muy similar a lo que en nuestros días es el Bloc
de Notas, TextEdit o Gedit.

# Primeros pasos

Al parecer, los editores de texto llegaron para aniquilar la idea de
«procesador de texto». No obstante, este concepto no sufriría la misma
suerte de IBM y su pérdida del monopolio por parte de compañías como
Xerox, Microsoft o Apple. Cuando los fabricantes de computadoras y
desarrolladores de *software* vieron el potencial comercial de un
programa que procesara texto —es decir, de un *software* que
enriqueciera a los editores de texto con un fin distinto a la
programación, como podría ser la elaboración de documentos
administrativos— empezó el boom de la era de la computación.

Los creadores de *hardware* y de *software* ya contaban con equipos a
bajos precios y con sistemas operativos capaces de realizar diversas
tareas de «cómputo», pero ¿cómo hacer que este producto no solo fuera de
interés para universidades o militares, sino también para el público en
general? Una vía fue hacer de las computadoras un medio de ocio y de
entretenimiento. Sin embargo, la estrategia que acarreó resultados
inmediatos fue hacer de las computadoras un equipo indispensable para
las oficinas. Aquí es donde el procesador de texto evolucionó y se
convirtió en un *software* que ayudó a popularizar las computadoras.

Rara vez se recuerda, pero fue gracias a las hojas de cálculo y al
procesador de texto cómo las computadoras se introdujeron masivamente,
tal vez no a todos los hogares, pero sí a la gran mayoría de las
oficinas. Al final, quienes tenían mayor poder adquisitivo no era el
público general, sino las empresas que requerían soluciones para sus
tareas administrativas. En este contexto, las incipientes empresas
tecnológicas crearían un lucrativo mercado: los paquetes de *software*
de ofimática.

A mediados de los ochenta las computadoras se convertían en la panacea
al traspapelo.

# De la oficina a la edición digital

Los inicios de los años noventa no podían ser más prometedores para el
usuario general o para quienes trabajaban detrás de un escritorio. La
paquetería de ofimática ya era un *software* imprescindible para
cualquier computadora. Las estrategias de negocios de Microsoft, las
cuales a finales de los noventas serían objeto de acciones legales por
prácticas monopólicas, hicieron de Microsoft Office el paquete por
excelencia y a Word el programa líder para el procesamiento de texto.

\[Imagen microsoftPublicidad.jpg con la leyenda: Publicidad de Microsoft
Office, 1990. Fuente:
[InfoWorld](https://books.google.com.mx/books?id=wFAEAAAAMBAJ&lpg=PA1&pg=PA50&redir_esc=y).\]

Los procesadores de texto trasladaron la mecanografía con creces. Ya no
solo se trataba de la introducción de texto, el *software* posibilitó
herramientas que en la actualidad se consideran innatas a cualquier
procesador: copiado y pegado, búsqueda y reemplazo, división del
documento por páginas, personalización de las características de la
página, inserción de imágenes; estilos directos que cambian el tamaño,
peso y tipo de fuente, etcétera. A pesar de ello, no todos los usuarios
estaban satisfechos. La calidad y el cuidado editorial dejaba mucho que
desear. Nadie dudaba que mediante el procesador de texto se creaban
documentos de oficina o trabajos escolares de calidad superior, pero
para la publicación de libros o artículos académicos esto representaba
un retroceso.

# Gestación paralela

¿Acaso los estándares de calidad de la tradición editorial no eran
compatibles con la revolución digital? Quienes editaban ¿debían ajustar
sus estándares y aceptar el tipo de trabajo realizado desde los
procesadores de texto? Desde finales de los setenta, Donald Knuth
percibió que el problema era una cuestión de enfoque. Este científico de
la computación e hijo de un pequeño impresor se sentía insatisfecho por
la calidad tipográfica de su libro *El arte de programar ordenadores*.
Para solventar el problema decidió desarrollar su propio lenguaje de
tipografía que culminaría en 1985.

[TeX](https://es.wikipedia.org/wiki/TeX) apareció en la escena con una
perspectiva muy distinta a los procesadores de texto. Mediante un
conjunto de [macros](https://es.wikipedia.org/wiki/Macro), como
[LaTeX](https://es.wikipedia.org/wiki/LaTeX) o
[ConTeXt](https://es.wikipedia.org/wiki/ConTeXt), TeX destaca por su
gran cuidado tipográfico y de composición de textos. En la actualidad,
es una herramienta muy popular para publicaciones académicas,
principalmente dentro del área de ciencias puras o de la informática. Su
calidad es tal que, a la fecha, solo se han introducido pequeñas
modificaciones.

Sin embargo, es innegable que fuera de esos ámbitos TeX no es la
herramienta más popular para la publicación. Desde su nacimiento, TeX se
vio opacado por su larga curva de aprendizaje y por tratarse de un
lenguaje de etiquetas, cuya escritura se asemeja a la redacción de
código. Si a esto sumamos que para poder visualizar el documento final
primero es necesario compilarlo, para la mayoría de las editoriales se
percibió —y se sigue viendo— como una vía poco factible.

# Analfabetismo digital

Si bien desde mediados de los ochenta ya existía una alternativa a los
procesadores de texto para quienes se dedicaban a la edición y
composición de documentos para su publicación, el usuario en general ha
tenido predilección por los procesadores. La cuestión no es de índole
económica o de cuidado editorial, ya que TeX, desarrollado
explícitamente para atajar estos retos y publicado como código abierto,
sería la alternativa más viable. El tema es de naturaleza pedagógica.

Uno de los sectores que han sido más reticentes a la revolución digital
ha sido el mundo de la edición. Sus sospechas no son injustificadas, el
procesamiento de texto no es una idea fruto de la tradición editorial,
sino que se generó como una iniciativa comercial de ciertas compañías
tecnológicas para ofrecer una mayor automatización del trabajo de
oficina.

\[Imagen processedworld12.jpg con la leyenda: Portada del décimo segundo
número de *Processed World*, 1984. Fuente: [Internet
Archive](http://archive.org/search.php?query=creator%3A%22Processed%20World%20Collective%22%20AND%20%28Processed%20World%29).\]

Nada puede ser más distante a las necesidades del cuidado editorial. No
obstante, este escepticismo no fue lo suficientemente consistente como
para frenar el traslado del quehacer editorial a una labor llevada a
cabo mediante *software*. En la actualidad, para agrado o desagrado, la
vasta mayoría de quienes se dedican a la redacción, edición y
publicación precisan de alguna computadora. Quien escribe se vale de un
procesador de texto para por lo menos transcribir su obra; quien compone
los textos emplea un *software* de maquetación para diagramar las
publicaciones; quien edita usa uno u otro programa para cumplir con su
trabajo; quien imprime se vale de maquinaria que interpreta los
documentos digitales de impresión.

Ser críticos hacia la tecnología y no concebirla como una panacea es una
postura admirable de varias personas que se dedican a la edición,
principalmente en mercados de habla hispana, en donde todavía existe un
fuerte nexo y respeto a la tradición editorial. Sin embargo, esta
actitud ha generado un profundo desconocimiento de las tecnologías
digitales, también conocida como «analfabetismo digital».

Para matizar un poco, la cuestión no reside en la adopción ingenua de
procedimientos tecnológicos que tal vez ayuden al cuidado de la edición:
esto ya aconteció desde finales de los noventa. El tema versa sobre el
empoderamiento del *hardware* y del *software* desde el cual se trabaja
y seguirá trabajando, lo cual también implica un uso crítico y
responsable de la tecnología que está al alcance.

TeX podría haber sido más afortunado si desde su gestación las
editoriales se hubieran puesto en la tarea de absorber los avances
tecnológicos para su beneficio. Incluso la gestación de una comunidad de
editores hubiese sido pertinente para demandar a los desarrolladores de
*software* la creación de programas a medida de sus necesidades
profesionales, o mejor, para generar sus propias herramientas. En su
lugar, a principios de los noventa se reprodujo el desconocimiento, la
falta de interés y el desdeño a algo que se percibía como innecesario.

Solo bastaron un par de años para que aquello secundario, poco a poco,
pasase a formar parte esencial del trabajo editorial. La suerte ya
estaba echada, la adopción aconteció demasiado rápido: el quehacer
editorial tuvo que buscar satisfacer sus necesidades a través de
herramientas pensadas para el trabajo de oficina. El aprendizaje de TeX
se mostró inviable debido a que el mundo de la edición no quiso darse el
tiempo suficiente para el cambio tecnológico. Lo que *ahora* conocemos
como «publicaciones digitales» aún no había nacido; sin embargo, la
edición digital daba sus primeros pasos.

# WYSIWYG y los lenguajes de etiquetas

En general, puede decirse que desde mediados de los ochenta surgieron
dos enfoques relacionados al tratamiento del texto digital:
[WYSIWYG](https://es.wikipedia.org/wiki/WYSIWYG) y los [lenguajes de
marcado](https://es.wikipedia.org/wiki/Lenguaje_de_marcado). El primero
es el acrónimo de *What You See Is What You Get*, con el cual se hace
patente que su aproximación estriba en dotarle al usuario de la mayor
flexibilidad posible en el formato de un texto, principalmente mediante
la introducción de estilos directos. Esta versatilidad no solo permite
al usuario visualizar inmediatamente sus modificaciones, sino que
también respeta el formato hasta su impresión. *Lo que ves en la
pantalla, es lo que obtienes en el papel*.

La otra aproximación consiste en dotarle al usuario de un gran control
sobre la estructura de un texto. Para ello es necesario la introducción
de *etiquetas*; a saber, un marcaje que permite delimitar unidades en la
estructura. Esta delimitación únicamente se pensó como una medida de
control en la apariencia de un texto, e implica que todavía hoy el
usuario debe marcar el documento cada vez que desee un cambio estético.
Debido a esta metodología, el usuario se ve en la necesidad de trabajar
sobre un documento que tiene que
[renderizar](https://es.wikipedia.org/wiki/Motor_de_renderizado) o
[compilar](https://es.wikipedia.org/wiki/Compilador) para poder ver su
resultado, donde las etiquetas desaparecen para dar lugar a los cambios
de aspecto.

\[Imagen wysiwygYlatex.jpg con la leyenda: Comparación del mismo texto
desde un enfoque WYSIWYG y de lenguaje de marcado LaTeX. Fuente:
[Wikimedia
Commons](https://commons.wikimedia.org/wiki/File:Lorem_Ipsum_-_WYSIWYG_en_Latex_-_tekst_als_paden.svg).\]

Durante los años noventa el lenguaje de marcado más desarrollado era
TeX. El incipiente surgimiento de la web trajo consigo el nacimiento del
HTML y, posteriormente, del XML, pero estos lenguajes de marcado no
fueron considerados para fines editoriales. (Quién diría que a inicios
del siguiente milenio estos se constituirán como las bases para las
publicaciones digitales). Durante esta década, quienes vivían de las
letras, por desconocimiento, pereza o premura, optaron por trabajar
desde un *software* que ya les era familiar. Escritores y editores
sentaron las bases para darle un uso distinto al procesador de texto. La
edición digital se inclinó al enfoque WYSIWYG que implementaba esta
clase de programas.

Otro argumento para la predilección del enfoque WYSIWYG es que la
presentación más madura y más afín al sector editorial del lenguaje de
marcado estaba pensado para la *composición* de textos, no para su
*edición*. Efectivamente, el quehacer del editor no es sencillo si se
hace directamente desde TeX. Por lo general, este lenguaje entra en
escena una vez que el editor da el visto bueno, permitiendo así pasar al
área de diagramación, en donde quizá existan ajustes, pero nada
significativo al trabajo realizado con anterioridad.

# *Desktop publishing*

Podríamos pensar que en el área de maquetación TeX se presentaba como la
alternativa más viable. Al final, este está pensado para el cuidado
tipográfico que, si bien es de conocimiento de quien edita, es el
tipógrafo o el diseñador editorial el experto sobre el tema. Pero el
desconocimiento de los recursos tecnológicos vino en detrimento a la
popularidad de TeX. El tipógrafo o el diseñador editorial por tradición
han basado su trabajo a partir del aspecto gráfico del texto; en cambio,
TeX había sido creado y concebido desde la mentalidad de un programador.

Para satisfacer la demanda de un *software* de composición de textos de
manera gráfica, nació la *desktop publishing* (DTP). A mediados de los
ochenta la empresa Aldus creó el primer programa para la maquetación de
documentos: [PageMaker](https://es.wikipedia.org/wiki/Adobe_PageMaker).
Este permitía un fácil traslado del trabajo realizado por el editor a un
ambiente de diseño editorial, ya que también implementaba un enfoque
WYSIWYG. Es curioso observar cómo 1985 fue el año del surgimiento del
*software* para la composición de documentos, ya que TeX y PageMaker
aparecieron durante ese tiempo.

\[Imagen pagemaker.jpg con la leyenda: Entorno de trabajo de PageMaker
7.0. Fuente: [Wikimedia
Commons](https://en.wikipedia.org/wiki/File:Adobe_PageMaker_7.0_on_Mac_OS_screenshot.png).\]

El paradigma del diseño editorial desde un entorno gráfico rápidamente
llamó la atención de otras empresas desarrolladoras de *software*, como
Adobe y Quarck Inc. En 1987 esta última lanzó al mercado
[QuarkXPress](https://es.wikipedia.org/wiki/QuarkXPress), un programa de
diagramación de textos superior al desarrollado por Aldus. Para ser más
competitivo, Adobe, empresa fundada en 1982 y orientada a la producción
de programas para el diseño gráfico, absorbió a Aldus en 1994. Para 1999
Adobe sacó a la luz su *software* de maquetación:
[InDesign](https://es.wikipedia.org/wiki/Adobe_InDesign).

Si bien es debatible la superioridad de InDesign sobre QuarkXPress, fue
la estrategia comercial de Adobe lo que permitió establecer a InDesign
como el programa *de facto* para la composición gráfica de documentos.
En 2003 se publicó la primera versión del [Adobe Creative
Suite](https://en.wikipedia.org/wiki/Adobe_Creative_Suite#Creative_Suite_1_and_2)
(CS1). Esta paquetería de *software* fue creada para responder a las
necesidades del quehacer gráfico. Entre todas las aplicaciones incluidas
destacan Illustrator, para el diseño gráfico con vectores; Photoshop,
para el diseño gráfico con mapa de bits, e InDesign para el diseño
editorial.

La estrategia no solo involucró la creación del CS1, por el cual se
creaban los archivos «editables», sino también la consolidación de
[PostScript](https://es.wikipedia.org/wiki/PostScript) y
[PDF](https://es.wikipedia.org/wiki/PDF#Versiones_del_formato_PDF) como
los formatos para la salida de impresión. Cabe recordar que Adobe
desarrolló ambos formatos, el primero en 1983 y el segundo, en 1993.
Para 2003 el PDF era el formato más popular para la impresión, por lo
que Adobe, en un solo paquete de *software*, absorbió todo el flujo de
trabajo necesario para el trabajo de diseño y, entre ellos, la
composición digital de textos.

La situación para el sector editorial no podía ser más prometedora. En
un lapso de quince años el mundo de la edición pasó del descalabro que
implicó la adopción de las tecnologías digitales a tener resuelto su
flujo de trabajo a través de dos paqueterías de *software*: Microsoft
Office para la redacción y edición de textos, y Adobe Creative Suite
para la diagramación y diseño de publicaciones.

A finales del milenio la edición digital se convirtió en la edición de
textos por excelencia.

# WYSIWYM

El principal flujo de trabajo implicado en la edición de publicaciones
no se encuentra exento de críticas. Al ser un método de producción
altamente dependiente del enfoque WYSIWYG, un problema recurrente en la
edición digital es la lucha a favor de la uniformidad en los estilos. El
descuido durante este método puede acarrear inconsistencias en la
publicación que van en detrimento del cuidado editorial. Ejemplos de
esto pueden ser:

-   Errores de estilo, como encabezados con la misma jerarquía pero con
    distinto tamaño de fuente, interlineado, tamaño de
    márgenes, etcétera.
-   Errores ortotipográficos, como párrafos con justificación forzada.
-   Errores en la creación de tablas de contenidos, referencias cruzadas
    o compaginación.

Con el fin de solventar este problema surgió el enfoque
[WYSIWYM](https://es.wikipedia.org/wiki/WYSIWYM), acrónimo de *What You
See Is What You Mean*. Con esto no solo se hace patente una oposición al
enfoque WYSIWYG, sino un retorno a los lenguajes de marcado para la
estructuración semántica del contenido. Las diferencias entre el enfoque
WYSIWYM y los lenguajes de marcado son que:

1.  el marcaje, aunque afecta el aspecto de un texto, tiene la función
    de segmentar la información de una manera adecuada y significativa
    para las herramientas de *software*, el editor y el lector, y
2.  la utilización de un editor gráfico, semejante al procesador de
    texto, que disminuye drásticamente la curva de aprendizaje.

El equipo de desarrollo de [LyX](https://es.wikipedia.org/wiki/LyX) fue
el que propuso este enfoque cuando creó este programa de edición gráfica
con LaTeX, en 1999.

\[Imagen lyx.jpg con la leyenda: Entorno de trabajo de LyX. Fuente:
[Wikimedia
Commons](https://commons.wikimedia.org/wiki/File:LyXScreen_Linux_en.png).\]

Para usuarios que no han tenido contacto con los lenguajes de marcado,
como lo es la mayoría de los escritores y editores, esta herramienta
puede resultar aún muy compleja. Por eso quizá una alternativa más
acorde a este contexto es el empleo de un [lenguaje de marcas
ligero](https://es.wikipedia.org/wiki/Lenguaje_de_marcas_ligero). La
diferencia entre un lenguaje «tradicional» de marcado y uno ligero es
que este último tiene una sintaxis de etiquetas más simple y limpia, que
resulta más fácil de aprender. Una propuesta en este sentido puede ser
[Nested Editor](http://nestededitor.sourceforge.net/about.html),
publicado en 2011.

\[Imagen nestededitor.jpg con la leyenda: Entorno de trabajo de Nested
Editor. Fuente: [Nested
Editor](http://nestededitor.sourceforge.net/media/images/nested.png).\]

Como es de observarse, el fin del milenio no solo fue importante para la
edición por el desarrollo de un ciclo de trabajo exclusivamente con
herramientas digitales, sino también por la propuesta de un nuevo
enfoque para el quehacer editorial que, pese a ser actualmente
minoritario, puede ser la clave para el futuro de la edición digital.

# ¿Y el *software* libre?

El principal flujo de trabajo tampoco ha sido visto con buenos ojos
desde la perspectiva del *software* libre. La dependencia tecnológica
del mundo editorial al *software* privativo, como el desarrollado por
Microsoft y Adobe, ha causado severas dudas sobre la «libertad» en la
edición. Los principales puntos de disputa han sido:

1.  El costo del *software* que para personas que se autopublican o
    pequeñas editoriales son imposibles de absorber.
2.  La falta de estandarización en los formatos que dificultan su
    conversión mediante herramientas desarrolladas por terceros.
3.  El carácter privativo de los formatos que acarrean una violación a
    la propiedad intelectual al intentarse abrir usando otros paquetes
    de *software*.
4.  La imposibilidad de personalización mediante la modificación del
    código fuente del *software* privativo.
5.  La dependencia a las decisiones tomadas por un comité ejecutivo, que
    pueden ir en oposición a los intereses editoriales y de
    los usuarios.
6.  La dependencia de la comunidad al servicio técnico ofrecido por los
    desarrolladores de *software* privativo.

Ante estas críticas, tanto Microsoft como Adobe, han flexibilizado sus
políticas. Como ejemplos tenemos la adopción de Word del formato
estandarizado [DOCX](https://es.wikipedia.org/wiki/Office_Open_XML)
(hasta 2003), los planes de renta mensual que permite un acceso a las
herramientas del Adobe Creative Suite o los descuentos ofrecidos a
estudiantes, profesores y universidades. Sin embargo, esta apertura no
se considera suficiente debido a que no garantizan las [cuatro
libertades](https://es.wikipedia.org/wiki/Software_libre#Las_cuatro_libertades_del_software_libre)
del *software* libre.

Por estos motivos, en lo que se refiere a la paquetería de ofimática, en
2002 se publicó
[OpenOffice](https://es.wikipedia.org/wiki/Apache_OpenOffice). En 2010
esta paquetería fue adquirida por [Oracle
Corporation](https://es.wikipedia.org/wiki/Oracle_Corporation). Esta
maniobra provocó sospechas entre la comunidad de *software* libre, por
lo que se llevó a cabo una bifurcación del proyecto para crear
[LibreOffice](https://es.wikipedia.org/wiki/LibreOffice). Para
consolidar la adopción de formatos abiertos y estandarizados para el
*software* de oficina, esta bifurcación también constituyó The Document
Foundation, cuyo principal cometido, además del mantenimiento y
mejoramiento de una paquetería de ofimática libre, es el cuidado y
popularización del
[ODF](https://es.wikipedia.org/wiki/OpenDocument#Aplicaciones_que_utilizan_el_formato_OpenDocument)
(*Open Document Format*, por su sigla en inglés).

\[Imagen libreoffice.jpg con la leyenda: Entorno de trabajo de Writer,
el procesador de texto de LibreOffice. Fuente: [Wikimedia
Commons](https://en.wikipedia.org/wiki/File:LibreOffice_Writer_5.1_Breeze.png).\]

En relación con una alternativa a Adobe Creative Suite, aún en la
actualidad no existe una opción que abarque todo el ciclo de trabajo que
contempla este paquete de *software*. En su lugar existen esfuerzos
independientes en los que se pueden mencionar los siguientes:

-   [Gimp](https://es.wikipedia.org/wiki/GIMP), publicado en 1996, como
    alternativa a Photoshop. Este *software* forma parte del proyecto
    [GNU](https://es.wikipedia.org/wiki/Proyecto_GNU) (la iniciativa
    para la creación de un sistema operativo libre).
-   [Inkscape](https://es.wikipedia.org/wiki/Inkscape), publicado en
    2003, como alternativa a Illustrator. Actualmente es la herramienta
    de editor de vectores recomendada por la
    [W3C](https://es.wikipedia.org/wiki/World_Wide_Web_Consortium) (el
    consorcio encargado de los estándares web).
-   [Scribus](https://es.wikipedia.org/wiki/Scribus), publicado en 2003,
    como alternativa a InDesign y QuarkXPress. Pese a su gran acogida
    entre la comunidad de *software* libre todavía en nuestros días
    carece de las posibilidades ofrecidas por su pares privativos.

\[Imagen scribus.jpg con la leyenda: Entorno de trabajo de Scribus.
Fuente: [Wikimedia
Commons](https://en.wikipedia.org/wiki/File:Scribus-1.3-Linux.png).\]

El año 2003 también fue importante para el mundo editorial, ya que el
*desktop publishing* pudo cerrar su ciclo de trabajo tanto en su opción
privativa como libre. Aunque dentro del ámbito editorial la versión
libre del DTP no goza de mucha popularidad, puede considerarse que es
posible utilizar herramientas libres para la edición digital desde un
enfoque WYSIWYG. Por último, solo cabe mencionar que el enfoque WYSIWYM
ha sido una propuesta llevada a cabo por la comunidad de *software*
libre, así como los lenguajes de marcado más populares, TeX y HTML, son
lenguajes abiertos.

# Publicación digital

La víspera del próximo milenio involucró el advenimiento de lo que
actualmente conocemos como «publicación digital». Aunque en otra entrada
se tratará con mayor detenimiento la historia de los *ebooks*, cabe
destacar que los primeros formatos pensados para su lectura desde
pantallas aparecieron en 1999.

A finales del milenio se publicaron las especificaciones de la OEBPS
(*Open eBook Publication Structure*, por su sigla en inglés). Esto sentó
las bases para el [Open eBook](https://en.wikipedia.org/wiki/Open_eBook)
y, posteriormente, para la OPS (*Open Publication Structure*, por su sigla
en inglés), la estructura del
[EPUB](https://en.wikipedia.org/wiki/EPUB). Otros formatos privativos y
populares como los empleados por
[Amazon](https://en.wikipedia.org/wiki/Amazon_Kindle#File_formats) o
[Apple](https://es.wikipedia.org/wiki/IBooks) han basado su desarrollo
en la estructura del EPUB o del Open eBook.

\[Imagen epub.jpg con la leyenda: Logotipo del EPUB, formato mantenido
por el International Digital Publishing Forum
([IDPF](http://idpf.org/)). Fuente: [Wikimedia
Commons](https://en.wikipedia.org/wiki/File:EPUB_logo.svg).\]

Si bien es cierto que el surgimiento de las publicaciones digitales
sentó un precedente en la edición digital, este no se debe al formato en
sí ni al concepto de publicación digital. La relevancia de las
publicaciones digitales reside en que hace patente repensar la
pertinencia del enfoque WYSIWYG dentro de los procesos editoriales, sin
importar que el formato de salida sea una publicación impresa o digital.

La idea de *publicación digital* existe mucho antes de la aparición de
los formatos desarrollados específicamente para cumplir esta función.
Así como el escritor y el editor empezaron a usar el procesador de texto
con fines distintos al trabajo administrativo, asimismo el usuario en
general empezó a utilizar ciertos formatos para otra función distinta; a
saber, la lectura a través de una pantalla. Los archivos de texto plano,
del procesador de texto, las páginas web y los PDF han sido empleados
como publicaciones digitales *de facto*.

En 1971 nació el [proyecto
Gutenberg](https://es.wikipedia.org/wiki/Proyecto_Gutenberg), la primera
iniciativa para crear una biblioteca digital a partir de libros
impresos. Nótese el nacimiento prematuro de la digitalización de obras
que, a pesar de ser una de las primeras directrices de los procesos
digitales de edición, aún hoy en día sigue considerándose un proceso
transversal que pocos editores le prestan atención. En la mayoría de los
casos, la digitalización se percibe más como un procedimiento técnico
que uno editorial.

\[Imagen proyectogutenberg.jpg con la leyenda: Logotipo del proyecto
Gutenberg. Fuente: [Wikimedia
Commons](https://en.wikipedia.org/wiki/File:Project_Gutenberg_logo.png).\]

Si se relacionan los procesadores de texto, la edición digital y el
libro electrónico puede decirse que: la idea con mayor antigüedad es la
del procesador de texto como *hardware* (años sesenta), en seguida la
del libro electrónico (años setenta), después la del procesador de texto
como *software* (años ochenta) y, por último, la de edición digital
(años noventa). La noción de libro electrónico es anterior a la de los
programas de procesamiento de texto y, evidentemente, más añeja que el
concepto de edición digital.

En los setenta, mientras que los primeros programadores centraban sus
esfuerzos en producir sistemas operativos cada vez más completos, los
científicos continuaban explorando el potencial de procesamiento de las
computadoras y los militares investigaban la pertinencia de esta
tecnología para fines bélicos. Michael Hart, un estudiante de la
Universidad de Illinois, utilizó una de las computadoras del Laboratorio
de Investigación de Materiales para transcribir la primera publicación
del proyecto Gutenberg: la Declaración de Independencia de los Estados
Unidos. Incluso Hart relata que esto aconteció el 4 de julio de 1971,
durante el aniversario de la independencia estadounidense… Cierto o no,
la anécdota demuestra cómo el optimismo y entusiasmo por crear una
biblioteca digital no precisó de la madurez tecnológica para crearla:
sin escáneres, sin procesadores de texto, solo un editor de texto plano
y tiempo libre.

El libro electrónico nació precipitadamente, sin respaldo tecnológico,
sin intereses lucrativos, solo con el puro afán de ofrecer libros de
manera digital y gratuita. Este espíritu, en mayor o menor medida, es la
explicación de por qué el libro electrónico existió treinta y ocho años
antes del surgimiento del primer formato desarrollado para satisfacer
esta necesidad. Pero ¿por qué tuvieron que pasar casi cuatro décadas
para el nacimiento de las *publicaciones digitales* como las conocemos
en la actualidad?

Al hacerse libros electrónicos con diversos formatos, como los
mencionados antes, no se percibió la necesidad de crear un archivo
*específico* para cumplir con esta tarea. Esto evidencia la herencia
tecnológica que ha acarreado la edición y la publicación en la era
digital: el uso de herramientas pensadas para otros fines y la
relevancia al aspecto visual en lugar de la estructura del contenido.

Los primeros libros electrónicos fueron elaborados con la ausencia de
resalte tipográfico. Posteriormente, estos fueron implementando
características ortotipográficas desde el enfoque WYSIWYG que permitían
los procesadores de texto. De nueva cuenta, los libros electrónicos
compuestos con TeX representaron una minoría. En la mayoría de los casos
estas circunstancias acarrearon una ausencia del cuidado editorial,
perceptible en errores en la transcripción o la digitalización, descuido
tipográfico, ausencia de composición y demás cuestiones en detrimento de
la legibilidad del texto.

Para combatir estas fallas, surgió la necesidad de un formato específico
para las publicaciones digitales cuya producción no involucrara un
enfoque WYSIWYG. Como el enfoque WYSIWYM no surgiría hasta principios
del milenio, los lenguajes de etiquetas fueron la respuesta a este
dilema. A finales de los noventa los lenguajes de marcado más
desarrollados eran TeX, HTML y XML, estos dos últimos creados para lo
que desde hace algunos años ya era una realidad: la
[*web*](https://en.wikipedia.org/wiki/World_Wide_Web).

\[Imagen primerapagina.jpg con la leyenda: Captura de pantalla del
explorador utilizado para la visualización de la [primera página
](http://info.cern.ch/hypertext/WWW/TheProject.html)[*web*](http://info.cern.ch/hypertext/WWW/TheProject.html).
Fuente:
[NPR](http://media.npr.org/assets/img/2013/05/21/screensnap2_24c_custom-991377675cc755770fee182040d4bc3d6d00ba69-s300-c85.gif).\]

Mientras el mundo editorial asimilaba el traslado digital de la mayoría
de su quehacer, el internet iniciaba su expansión más allá de los
límites académicos y militares. El surgimiento del internet, en donde
curiosamente el Laboratorio de Investigación de Materiales de la
Universidad de Illinois formó parte de los primeros nodos, produjo el
nacimiento de la web en 1991. Esta utilizaba como archivos de
intercambio los documentos de hipertexto.

El lenguaje HTML nació como el medio para crear marcas de hipertexto. La
rápida evolución de la web propició que el HTML se consolidara como uno
de los lenguajes de marcado más populares, cuya principal característica
es la posibilidad de presentar inconsistencias en la sintaxis sin
ocasionar graves problemas durante la visualización. Esta flexibilidad
puede ocasionar dificultades al momento de utilizar este lenguaje de
marcado como un archivo para el procesamiento automatizado de
información. Con el fin de solucionar este inconveniente, se creó el
[XML](https://es.wikipedia.org/wiki/Extensible_Markup_Language), un
lenguaje de marcado más estricto. Entre uno y otro formato, en el 2000
nació un lenguaje de marcado para su visualización, tal como el HTML,
que exige una sintaxis clara, como el XML; a saber, el
[XHTML](https://es.wikipedia.org/wiki/XHTML).

Este conjunto de formatos, todos bajo el cuidado del W3C, permitieron
una rápida propagación del internet gracias a su capacidad de transmitir
información de una manera clara para el usuario y consistente para las
computadoras. Esta doble ventaja no pasó desapercibida ante quienes
buscaban crear libros electrónicos desde un enfoque distinto al WYSIWYG.

A partir de 1999 y principalmente desde el 2007, cuando sale a la luz la
primera versión de EPUB, las publicaciones digitales estandarizadas
contemplan las siguientes características mínimas que permiten la
creación de un libro electrónico más versátil y de mayor calidad
editorial:

-   El uso del lenguaje XML para la creación de la estructura y
    metadatos del libro. Por ejemplo, para indicar el título y autor de
    la obra, manifestar los archivos existentes en el libro, señalar el
    orden de lectura o crear la tabla de contenidos.
-   El uso del lenguaje XHTML, aunque también el HTML es soportado, para
    el contenido del libro. Por ejemplo, los archivos para el prólogo,
    los capítulos, el epílogo o los apéndices.
-   La compresión de este árbol de directorios para crear un solo
    archivo que fácilmente puede compartirse.

\[Imagen sexochilango.jpg con la leyenda: Captura de pantalla de la
estructura y el contenido de *Sexo Chilango*, un libro publicado por Nieve
de Chamoy. Fuente: [Nieve de
Chamoy](http://www.nievedechamoy.com.mx/sitio/).\]

Estos formatos de publicación digital en sí mismos demuestran ser los
más óptimos, porque por el uso del lenguaje de marcado permite que los
lectores de libros organicen de una mejor manera el contenido al mismo
tiempo que el lector tiene la posibilidad de enriquecer su experiencia
de lectura. Sin embargo, en el mundo editorial su acogida no fue como se
esperaba. Algunos de los factores que permiten explicar la tardía o
desinteresada recepción de la publicación digital dentro de los procesos
digitales de edición pueden ser:

1.  la continua disputa entre la lucha de formatos: impreso *vs*
    digital,
2.  el desdén ocasionado por la baja calidad que presentan muchos libros
    electrónicos,
3.  el costo adicional que implica la producción de un libro electrónico
    o
4.  la reticencia de modificar los procesos de edición digital.

En futuros artículos se hablará sobre los primeros dos puntos. Respecto
a los puntos restantes, efectivamente para la gran mayoría de las
editoriales la creación de un libro electrónico involucra una inversión
adicional ya que la metodología empleada para la producción de libros
impresos dista mucho del manejo de etiquetas necesario para el
desarrollo de *ebooks*. Para que la publicación digital forme parte
«natural» dentro de los procesos de edición digital es necesario que
estos paulatinamente dejen de lado el enfoque WYSIWYG.

La suspicacia ante dicho traslado es comprensible. Durante los noventa
el sector editorial se vio en la necesidad de adoptar la edición digital
como el principal método para la producción de libros. Una década
después se le indica al mundo del libro que de nueva cuenta ha de acoger
otra metodología para la publicación de libros, sean impresos o
digitales.

\[Imagen procesoseditoriales.jpg con la leyenda: Esquema de los
principales procesos editoriales, en horizontal, los considerados
fundamentales y en vertical, los estimados como transversales. Fuente:
[Perro Triste](https://github.com/ColectivoPerroTriste).\]

De manera paralela a esta discusión endémica al mundo de la edición,
grandes distribuidores de productos digitales —algunos de ellos
completamente ajenos a la comercialización de libros, como Google o
Apple, u otro con explícitas intenciones de modificar el mercado del
libro, como Amazon— han apostado desde hace algunos años por el libro
electrónico. Esta intromisión no es mínima: al tiempo que se modifican
las reglas tradicionales para la venta de libros también ha cambiado la
forma de interacción del lector con el escritor y de este con el editor.
De modo paralelo, las editoriales se han visto en la necesidad de
publicar libros electrónicos a marchas forzadas para satisfacer este
nuevo mercado.

Bajo este panorama, quienes mejor han logrado adaptarse a este cambio
son los jóvenes lectores y las personas que se autopublican. La primera
década del nuevo milenio puede decirse que fue el inicio de la lucha del
sector editorial con los fantasmas de su pasado.

# *Single source publishing*

¿Hacia dónde puede dirigirse la edición digital? El abandono del enfoque
WYSIWYG es necesario en aras de la disminución del tiempo empleado para
producir una publicación, sea impresa o digital. Este cambio no tiene
necesidad de ser drástico, sino acorde a la profundización del
conocimiento tecnológico por parte del editor y a la implementación de
composición de textos ajenos al WYSIWYG.

Desde hace algunos años los procesadores de texto usuales, como Word o
Writer, ya ofrecen un enfoque similar al manejo de etiquetas. Dentro de
estos programas, este planteamiento se conoce como *estilos de párrafo y
de caracteres*. Mediante el empleo de estos estilos cabe la posibilidad
de marcar semánticamente el contenido al que, posteriormente, puede
exportarse a HTML, para así continuar su procesamiento para la creación
de publicaciones digitales.

Los *software* de maquetación también ofrecen la posibilidad del manejo
de estilos, los cuales pueden importarse a partir de documentos de
procesadores de texto. Asimismo, existe la posibilidad de importar
documentos HTML, en el caso de Scribus, o XML, en el caso de InDesign.
Por último, esta clase de programas también ofrece la posibilidad de
exportar el documento a HTML o EPUB.

\[Imagen estiloparrafo.jpg con la leyenda: Panel de los estilos de
párrafo en InDesign. Fuente:
[AdobePress](http://www.adobepress.com/articles/article.asp?p=1873031).\]

Los desarrolladores de *software* son conscientes de la necesidad actual
de publicar un libro en diversos formatos. Por este motivo, son ellos
los que están proponiendo nuevas metodologías para el tratamiento del
texto. La conversión entre formatos es incuestionable y un ejemplo
formidable de esta posibilidad es
[Pandoc](https://en.wikipedia.org/wiki/Pandoc), un *software* publicado
en 2006 que es capaz de convertir documentos en más de treinta formatos
posibles, entre los que sin duda se incluyen los que han sido
mencionados a lo largo de este texto.

La conversión llegó para ocupar un lugar especial dentro de la edición
digital, pero solo es una parte de la posible metodología de trabajo.
Comúnmente quienes editan perciben a la conversión como la solución a su
necesidad de publicar el libro en diversos formatos. El alivio pronto es
opacado cuando los archivos de salida se visualizan de manera indeseada,
siendo menester la intervención directa y caso por caso que, a la larga,
aumenta los tiempos de producción o, en el peor de los casos, causa
frustración al editor.

El problema no reside en la conversión en sí, sino en los archivos
originales de los que se parte. Los archivos creados por los
procesadores de texto y por los maquetadores, en la mayoría de los
casos, son demasiado complejos internamente como para que a través de su
conversión se obtengan los resultados esperados. Existe la necesidad de
partir de formatos más simples, pero que al mismo tiempo no pierdan de
vista la estructura semántica de la obra.

Para simplificar el formato primero es necesario separar, de una vez por
todas, el contenido del diseño. A través de los años, las tecnologías
web han mostrado la pertinencia de dividir el documento en sus
diferentes dimensiones. Si hablamos de una página web estática, es
decir, que no tiene interacción alguna con un servidor, este consta de
tres dimensiones:

1.  el contenido en formato HTML que da estructura,
2.  el diseño en formato CSS que modifica su apariencia visual y
3.  la programación en Javascript para dotarle de funcionalidad
    al documento.

\[Imagen tecnologiasweb.jpg con la leyenda: Las tres dimensiones de una
página *web*. Fuente: [Beuco](http://becuo.com/html-css-javascript).\]

La tercera dimensión, como puede anticiparse, es el terreno del
programador, mientras que la segunda es el campo del diseñador. Sin
embargo, en lo que respecta a la primera dimensión, la responsabilidad
es compartida entre el maquetador y el editor. Será en otro artículo en
donde se hable de la posible pertinencia de crear una dimensión
exclusiva para el editor, el cual no tenga que preocuparse de la
estructura necesaria para los archivos HTML.

La división de trabajo entre el diseño editorial y la edición ha estado
presente desde hace varios siglos dentro del quehacer editorial. Pero
dentro de la edición digital esta delimitación ha quedado difusa, hasta
el punto donde el editor, e incluso el escritor, «diseña» el texto a
partir de un procesador. Para un mejor desempeño, en la edición digital
es pertinente un enfoque exclusivo en la estructuración y edición del
contenido. Con el fin de cumplir este cometido, la ausencia de diseño es
pertinente.

Esto nos lleva a una segunda recomendación para la simplificación del
formato: el editor ha de emplear un lenguaje de etiquetado ligero o
trabajar a través de un editor WYSIWYM cuyo formato de salida tenga esta
clase de etiquetado. Un lenguaje de etiquetado ligero tiene lo básico
para la estructura de un documento: itálicas, negritas, encabezados,
párrafos, bloques de citas, citas dentro de párrafos, listas numeradas o
no numeradas, enlaces a imágenes, hipervínculos y hasta tablas. Aprender
a realizar este marcado no es una tarea difícil.

En nuestros días, el lenguaje de marcado más popular es, sin dudas,
[Markdown](https://es.wikipedia.org/wiki/Markdown). La idea detrás de
este lenguaje es la facilidad en su lectura, en su escritura y en su
conversión a documentos HTML. Desde 2004 Markdown fue ganando
popularidad porque rápidamente fue adoptado para la redacción de
documentación de los
[repositorios](https://es.wikipedia.org/wiki/Repositorio) de *software*.
A partir de ahí, este lenguaje ha empezado a emplearse como el archivo
originario para la conversión a otros formatos además del HTML, como lo
es el XML, PDF, TeX o EPUB.

\[Imagen markdown.jpg con la leyenda: Comparación del mismo texto, a la
izquierda su escritura con Markdown y, a la derecha, su
previsualización. Fuente: [Wikimedia
Commons](https://es.wikipedia.org/wiki/Markdown#/media/File:Haroopad_Markdown_editor.png).\]

Con estas dos recomendaciones se cubren las necesidades de una gran
cantidad de publicaciones. Sin embargo, para las obras que contienen
notas al pie o bibliografía existe una sugerencia adicional: separar el
contenido de las notas y la bibliografía del cuerpo principal. Esta
metodología se ha estado aplicando en LaTeX para el manejo de
bibliografía, por las cuales las obras referidas se encuentran en una
base de datos creada con herramientas como
[BibTeX](https://es.wikipedia.org/wiki/BibTeX).

A partir de una simple marca con un identificador, LaTeX tiene la
capacidad de agregar la referencia al momento de compilar el documento.
Esto no solo facilita el trabajo al escritor, sino también el del
editor, ya que se elimina de una vez por todas el problema de la falta
de uniformidad en la bibliografía, al convertirla con estilos
personalizables.

\[Imagen bibtex.jpg con la leyenda: Ejemplo de uso de BibTeX y LaTeX
para producir un PDF con bibliografía: en BibTeX se crea un
identificador a la referencia (círculo superior derecho) que se coloca
adentro de una marca de LaTeX (círculo izquierdo) para producir una
referencia en el PDF al estilo APA (círculo inferior derecho); es
posible otro estilo sin necesidad de modificar la ficha bibliográfica
(rectángulo). Fuente: [University College
Cork](http://publish.ucc.ie/doc/postgrad-intro?sectoc=2).\]

Para las notas al pie puede darse el mismo caso. El escritor solo habría
de preocuparse por marcar el lugar donde va la nota, mientras que el
editor se enfoca en revisar su contenido sin la necesidad de preocuparse
por su numeración. Aunque parezca sorprendente, la separación entre las
notas y el cuerpo principal es tan infrecuente que ni siquiera TeX tiene
contemplada esta posibilidad. En su lugar se ha dado preferencia a
incluir el contenido de la nota, debidamente marcada, en el texto
principal.

Semejante práctica no es problemática siempre y cuando se trabaje desde
entornos que facilitan la tarea de adición de notas, como lo es TeX. No
obstante, esto no se recomienda para documentos que han de convertirse a
XML o HTML, ya que las notas al pie en estos archivos se dan como
referencia cruzada. En las páginas web o publicaciones digitales, el
contenido de las notas se encuentra afuera del párrafo donde se ubica el
número de la nota. Por ello, es necesaria la creación de hipervínculos
entre el contenido de la nota y su número.

La creación de un documento «madre» que incluya las notas dentro del
texto principal es posible. No obstante, es más sencillo mantener la
separación, como en el caso del HTML o XML, y unirlos si es necesario,
como es el caso de LaTeX. Además, la división también ayuda a clarificar
la tarea del editor, debido a que así se evita la confusión entre el
contenido de la nota y el contenido del texto principal. En el caso de
un grupo de editores, uno puede enfocarse en el contenido principal,
otro en las notas y uno más en la bibliografía, evitándose la
intromisión entre ellos.

Estas tres recomendaciones producen un documento en marcaje ligero del
contenido principal, una base de datos para la bibliografía (opcional) y
una relación de notas (opcional), las que, mediante conversores, es
posible la generación de distintos formatos para su publicación sin la
frustración de obtener resultados inesperados. Como es patente, todas
estas prácticas hace años que están presentes en el cuidado y
composición digital de documentos. Sin embargo, la conjunción de estos
cuidados en el formateo es lo que se conoce como [*simple source
publishing*](https://en.wikipedia.org/wiki/Single_source_publishing).

Es decir, la idea de un simple formato «madre» no implica una pérdida de
calidad, sino una división del trabajo y de la obra. Por un lado, el
libro es dividido en su contenido principal, su bibliografía y sus
notas, haciendo que quien edita se concentre en la estructura y edición
principal del libro, en la revisión de la bibliografía y en la edición
de las notas de manera independiente.

Por el otro, el editor realiza su trabajo y al dar el visto bueno, el
diseñador editorial, tipógrafo o programador inician la labor de crear
una publicación impresa o digital. Si existen ajustes, estos no han de
ser graves. Si lo son, solo es necesario volver a importar el contenido
cuyo aspecto visual no se pierde, debido al control existente entre las
etiquetas y sus estilos.

Semejante flexibilidad es la que permite utilizar el mismo archivo
«madre» a través de diversos formatos. Podría pensarse que esta
metodología es de difícil implementación en el estado actual de la
edición digital. Sin embargo, es solo una cuestión de adopción de
formatos en el momento adecuado. En otros términos:

-   El autor se dedica a lo suyo y entrega al editor su «archivo final»,
    por lo general un archivo elaborado a través de un procesador
    de textos.
-   El editor divide la obra en su contenido principal, las notas y
    la bibliografía. A continuación, en lugar de trabajar con el
    procesador de textos, utiliza un lenguaje de marcado ligero o un
    editor WYSIWYM; marca y da formato a las notas, así como también
    marca y genera la base de datos de la bibliografía. Por último, se
    dedica a editar la obra de la manera habitual. Una vez terminado se
    obtiene el «archivo madre», el cual está bajo su control y cuidado.
-   Del archivo madre se convierte a HTML o XML para que el diseñador
    editorial trabaje con su maquetador preferido. Al venir el texto de
    un lenguaje de marcado, de una manera sencilla puede asociar las
    etiquetas a estilos de párrafo, por lo que deja de preocuparse por
    la estructura de la obra, para concentrarse en su contenido y de los
    ajustes ortotipográficos propios de una publicación impresa.
-   Del archivo madre se convierte a LaTeX para el diseñador editorial o
    tipógrafo que prefiere trabajar con TeX. No existe preocupación
    sobre su estructura, por lo que la persona responsable se encarga de
    compilar y, si es necesario, de realizar pequeños ajustes en
    el documento.
-   Del archivo madre se convierte a HTML o XHTML para que el
    desarrollador genere una publicación estandarizada en formato EPUB.
    A partir de una serie de *scripts* el responsable ya no tiene
    necesidad de cuidar la estructura del EPUB más allá de la
    introducción de metadatos, ajustes en el diseño mediante estilos CSS
    y, si se requiere, la conversión de este formato a otros privativos,
    como el formato MOBI de Amazon a través de
    [KindleGen](http://wiki.mobileread.com/wiki/KindleGen).

\[Imagen flujoepub.jpg con la leyenda: Propuesta de Perro Triste para la
automatización en la creación de EPUB. Fuente: [Perro
Triste](https://github.com/ColectivoPerroTriste/Herramientas/tree/master/EPUB).\]

Con la metodología *simple source publishing* el editor es el único que
debe modificar su manera de trabajar en aras de tener un mayor control
sobre la edición. Si bien es muy temprano para anticipar que este método
puede ser el más óptimo para el quehacer editorial, sus posibilidades
son prometedoras, por lo que la experimentación es recomendable.

# *Online publishing*

Para tener aún mayor control sobre la edición, existe la posibilidad de
que el proyecto de una obra tenga un sistema de [control de
versiones](https://es.wikipedia.org/wiki/Control_de_versiones). ¿Cuántas
veces no se han colado ediciones antiguas a un archivo de salida?
¿Cuántas veces no se han encontrado errores que previamente se habían
corregido? ¿Cuántas veces las versiones «finales» son tantas que existen
coletillas en los archivos como «versión-final-final02»?

La pesadilla de no poder encontrar la versión actual de un archivo,
hasta el punto de tener que meter los cambios de nueva cuenta, también
ha sido un dolor de cabeza para los desarrolladores de *software*. Para
solucionar esta dificultad, se crearon sistemas de fácil uso pero muy
potentes para controlar cambios. Mediante estos, el desarrollador y el
editor pueden trabajar de manera habitual con sus archivos.

Cada vez que se realiza algo importante, como la conclusión de una tarea
o una modificación crucial, este cambio puede ser guardado, que en la
jerga se conoce como *commit*. Si se desea experimentar un poco, es
posible crear una línea de trabajo paralela, llamadas «ramas». Si se
obtiene el resultado esperado es posible la incorporación al trabajo
principal y si se consiguen consecuencias indeseadas se puede regresar a
la rama principal y eliminar la experimental. Por último, si durante la
edición o el desarrollo algo sale mal, siempre es posible retornar a
versiones previas sin la necesidad de haber hecho un respaldo de los
archivos.

\[Imagen gitramas.jpg con la leyenda: Representación gráfica de un
sistema de control de versiones. Los puntos representan cambios
guardados, mientras que las líneas son las diferentes líneas de trabajo
sobre un mismo proyecto. Fuente:
[Git-it](http://jlord.us/git-it/challenges/branches_arent_just_for_birds.html).\]

Tal vez la comprensión de esta característica puede ser difícil para el
editor habitual. Lo importante a destacar aquí es que un sistema de
control de versiones permite:

1.  que el editor o diseñador editorial deje de respaldar archivos, para
    que en su lugar genere puntos de guardado en su proyecto, y que
2.  el proyecto con un sistema de esta clase pueda estar disponible en
    línea para un trabajo colaborativo no presencial.

El quehacer editorial en varios casos exige la coordinación de un grupo
de trabajo que, por lo general, se presenta a inconvenientes debido a
las agendas de los diferentes colaboradores. El envío de correos y de
archivos adjuntos ha sido la solución estándar ante esta dificultad. No
obstante, esto en más de una ocasión ha dado fruto a confusiones, como
la descarga de versiones viejas, la pérdida de correos, etcétera. Otra
alternativa a este inconveniente es posible si el proyecto, junto con el
sistema de control de versión, que en conjunto se denomina
«repositorio», se sube a un servidor para que cada colaborador esté
actualizando sus archivos locales o subiendo sus modificaciones.

\[Imagen gitrepositorio.jpg con la leyenda: Representación gráfica de
un repositorio en el servidor y los repositorios locales que se
actualizan o modifican este repositorio remoto. Fuente: [Terminally
Incoherent](http://www.terminally-incoherent.com/blog/2012/05/07/the-plight-of-a-git-newb/).\]

Esta característica es muy similar a tener archivos en la nube. La
diferencia reside en que un repositorio cuenta con un potente sistema de
control de versiones.

Esta mediación del internet para la consecución exitosa de un proyecto
editorial ha generado la idea de la *online publishing*. La publicación
en línea no solo hace referencia a que las obras estén disponibles en la
web, sino que el internet sea un intermediario para crear un flujo de
trabajo controlado y sistemático. Además, esta concepción es muy afín al
*simple source publishing*, ya que implica la posibilidad de control
remoto a la metodología de producción de un archivo base.

En la actualidad, existen plataformas que ya ofrecen ambas
características. Un ejemplo es [Bookdown](https://bookdown.org/) que,
como dice su eslogan, permite la creación de libros en formatos HTML,
PDF, EPUB o para Kindle desde Markdown. Otro ejemplo es
[Leanpub](https://leanpub.com/) que, además de permitir la diversa
salida de formatos a partir de archivos Markdown presentes en
repositorios, pone en práctica la idea de «publicación progresiva». Es
decir, la posibilidad de actualizar indefinidamente un libro al unísono
que el lector obtiene la versión más reciente de la obra.

Otro ejemplo más es [GitBook](https://www.gitbook.com/about), un
proyecto de [GitHub](https://es.wikipedia.org/wiki/GitHub), la
plataforma más popular para alojar repositorios
[Git](https://es.wikipedia.org/wiki/Git). Este sitio permite la
publicación digital con [diseño
responsivo](https://es.wikipedia.org/wiki/Diseño_web_adaptable) a partir
de archivos Markdown. Al estar la obra en un repositorio, su
actualización se refleja en cuestión de segundos, posibilitando así la
idea de la publicación progresiva.

\[Imagen gitbook.jpg con la leyenda: [*Guía de Nieve de
Chamoy*](https://nikazhenya.gitbooks.io/guia-de-nieve-de-chamoy/content/),
una publicación hospedada en GitBook. Fuente:
[GitBook](https://www.gitbook.com/).\]

# Balance del análisis

La automatización y sistematización de la edición digital ya no es un
sueño, sino una posibilidad. No obstante, como se ha reflejado a lo
largo de este artículo, su consecución requiere que:

1.  la edición acoja un enfoque distinto al WYSIWYG, y que
2.  el sector editorial analice la posibilidad de optar por otras
    metodologías de edición digital, sin que esto represente un traslado
    radical en la composición de documentos.

La suspicacia ante estos cambios es aceptable: la relación entre el
desarrollo tecnológico y la tradición editorial ha producido más
desencuentros que puntos de acuerdo. Para agrado o desagrado, en la
actualidad la publicación digital no es una opción, sino un requisito
para la comercialización de libros. Al final, la conservación o
desaparición de una editorial es proporcional a su capacidad de vender
sus obras.

El instinto de supervivencia en algunas ocasiones ha llevado a un
enfoque cuantitativo y efímero dentro de la publicación que, a su vez,
implica una disminución o pérdida del cuidado editorial. La tecnología,
como cualquier otra herramienta, puede ser empleada con este fin o para
la generación de otras alternativas.

A lo largo de las décadas el sector editorial ha tenido la oportunidad
de absorber el desarrollo tecnológico para su beneficio y bajo sus
exigencias. Sin embargo, el desdén o falta de vinculación han propiciado
que el quehacer editorial tenga que adaptarse a herramientas creadas
para otros fines, conllevando la generación de dificultades que en la
actualidad se consideran «naturales» a la edición digital. Ejemplos de
estos inconvenientes son la falta de uniformidad, el poco control sobre
la edición, las tortuosas tareas de generar tablas de contenidos, de
ordenar bibliografía o de agregar notas al pie y demás cuestiones que
implican un *formateo* del texto.

Al respecto, vale la pena resaltar que existe una diferencia crucial
entre «formato» y «formateo» de un texto. El primero hace referencia a
la «forma», al modo en cómo un documento es presentado, mientras que el
segundo es una alusión a la necesidad de modificar esta «forma» en aras
de conseguir la presentación deseada. Esta cuestión no es nimia: es el
cuello de la botella dentro de la edición digital.

Dicho problema es de tal envergadura que, por lo expuesto con
anterioridad, cabe la posibilidad de argumentar que se trata del
nacimiento de la edición digital. La diferencia específica de este modo
de edición no solo es el empleo del *software*, sino el uso de estas
herramientas para enmendar un formato que nunca ha sido el adecuado para
el cuidado editorial.

\[Imagen formateo.jpg con la leyenda: El *formateo* como el proceso más
lento dentro de la edición digital.\]

El *formateo* es una noción inexistente o muy infrecuente en la edición
predigital ya que esta iba de la mano a la creación del formato al
texto; la necesidad de volver a repetir este proceso solo era en el caso
de una pésima composición. En cambio, en la edición digital el archivo
dado por el autor ya contiene un formato poco uniforme y predeterminado
por un procesador de textos que crea la necesidad de limpiar ese formato
para lograr el adecuado.

Por este motivo se dice que la edición digital antecede a la publicación
digital, pero al mismo tiempo viene después del libro electrónico. El
ebook irrumpió sin prestarle mucha atención al cuidado editorial: su
objetivo era la disponibilidad en línea. La edición digital nació de la
necesidad de adecuar documentos para sus propias necesidades, mientras
se hallaba sumergida en la precipitada adopción tecnológica. Por último,
la publicación digital se constituye como el elemento que expuso los
vicios y posibilidades de cambio de la edición digital convencional.

Esta exhibición de fallas es la que ha hecho que la edición y la
publicación digital sean un tema de debate actual, incluso hasta el
punto de pensar que ambas tienen una misma o cercana génesis histórica o
incluso que se trata de sinónimos. Ojalá que con este artículo quede
evidenciado que la edición digital tiene un origen muy distante: fue
cuando el editor empezó a utilizar el *software* y a formatear que la
edición digital cobró vida, independientemente de que en un principio su
intención fuera la de publicar libros impresos.
